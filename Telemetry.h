#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace std;

const int SIZE = 100;

struct Node 
{
	string name;
	string type;
	int8_t *data8;
	int16_t *data16;
	int32_t *data32;
	int64_t *data64;
	double *dataD;
};

class Telemetry 
{
	private:
		static bool instanceFlag;
		static Telemetry *ptr;
		static int count;
		Telemetry ();
		Node dataSet[SIZE];

	public:
		static Telemetry* getInstance ();
		~ Telemetry ();

		void tele (string inputTxt);

		void addVar (string key, int8_t *val);
		void addVar (string key, int16_t *val);
		void addVar (string key, int32_t *val);
		void addVar (string key, int64_t *val);
		void addVar (string key, double *val);

		void getVal (string key);
		void setVal (string key, double val);

		void print ();
};








bool Telemetry :: instanceFlag = false;

Telemetry* Telemetry :: ptr = NULL;

int Telemetry :: count = 0;

Telemetry :: Telemetry () 
{
	for (int i = 0; i < SIZE; i++) 
	{
		dataSet[i].name = "\n";
		dataSet[i].type = "\n";
	}
}

Telemetry* Telemetry :: getInstance () 
{
	if (!instanceFlag) 
	{
		ptr = new Telemetry ();
		instanceFlag = true;
		return ptr;
	}
	else 
	{
		return ptr;
	}
}

Telemetry :: ~Telemetry () 
{
	instanceFlag = false;
}

void Telemetry :: tele (string inputTxt)
{
	char console[50] = {' '};
	char tele[10] = {' '};
	char oper[10] = {' '};
	char temp[10] = {' '};
	double val = 0;
	string key;

	strcpy (console, inputTxt.c_str());
	printf ("%s \n", console);
    sscanf (console, "%s %s %s %lf", tele, oper, temp, &val);
    key = temp;

    if (!strcmp (tele, "tele"))
    {
    	if (!strcmp (oper, "get")) 
    	{
    		getVal (key);
    	}

    	else if (!strcmp (oper, "set")) 
		{
			setVal (key, val); 
		}

    	else if (!strcmp (oper, "print")) 
		{
			print ();
		}
    }
}

void Telemetry :: addVar (string key, int8_t *val)
{
	if (count < SIZE)
	{
		dataSet[count].name = key;
		dataSet[count].data8 = val;
		dataSet[count].type = "8";
		printf (">%s:	%i \n", dataSet[count].name.c_str(), *dataSet[count].data8);
		count++;
	}
}

void Telemetry :: addVar (string key, int16_t *val)
{
	if (count < SIZE)
	{
		dataSet[count].name = key;
		dataSet[count].data16 = val;
		dataSet[count].type = "16";
		printf (">%s:	%i \n", dataSet[count].name.c_str(), *dataSet[count].data16);
		count++;
	}
}

void Telemetry :: addVar (string key, int32_t *val)
{
	if (count < SIZE)
	{
		dataSet[count].name = key;
		dataSet[count].data32 = val;
		dataSet[count].type = "32";
		printf (">%s:	%i \n", dataSet[count].name.c_str(), *dataSet[count].data32);
		count++;
	}
}

void Telemetry :: addVar (string key, int64_t *val)
{
	if (count < SIZE)
	{
		dataSet[count].name = key;
		dataSet[count].data64 = val;
		dataSet[count].type = "64";
		printf (">%s:	%ld \n", dataSet[count].name.c_str(), *dataSet[count].data64);
		count++;
	}
}

void Telemetry :: addVar (string key, double *val) 
{
	if (count < SIZE)
	{
		dataSet[count].name = key;
		dataSet[count].dataD = val;
		dataSet[count].type = "D";
		printf (">%s:	%f \n", dataSet[count].name.c_str(), *dataSet[count].dataD);
		count++;
	}
}




void Telemetry :: getVal (string key)
{
	for (int i = 0; i < SIZE; i++)
	{
		if (dataSet[i].name == key)
		{
			if (dataSet[i].type == "8")
			{
				printf (">%i  \n", *dataSet[i].data8);
			}
			else if (dataSet[i].type == "16")
			{
				printf (">%i  \n", *dataSet[i].data16);
			}
			else if (dataSet[i].type == "32")
			{
				printf (">%i  \n", *dataSet[i].data32);
			}
			else if (dataSet[i].type == "64")
			{
				printf (">%ld  \n", *dataSet[i].data64);
			}
			else if (dataSet[i].type == "D")
			{
				printf (">%f  \n", *dataSet[i].dataD);
			}
		}
	}
}

void Telemetry :: setVal (string key, double val)
{
	for (int i = 0; i < SIZE; i++)
	{
		if (dataSet[i].name == key)
		{
			if (dataSet[i].type == "8")
			{
				*dataSet[i].data8 = (int8_t)val;
				printf (">%i \n", *dataSet[i].data8);
			}
			else if (dataSet[i].type == "16")
			{
				*dataSet[i].data16 = (int16_t)val;
				printf (">%i \n", *dataSet[i].data16);
			}
			else if (dataSet[i].type == "32")
			{
				*dataSet[i].data32 = (int32_t)val;
				printf (">%i \n", *dataSet[i].data32);
			}
			else if (dataSet[i].type == "64")
			{
				*dataSet[i].data64 = (int64_t)val;
				printf (">%ld \n", *dataSet[i].data64);
			}
			else if (dataSet[i].type == "D")
			{
				*dataSet[i].dataD = val;
				printf (">%f \n", *dataSet[i].dataD);
			}
		}
	}
}

void Telemetry :: print ()
{
	for (int i = 0; i < SIZE; i++)
	{
		if (dataSet[i].name != "\n")
		{
			if (dataSet[i].type == "8")
			{
				printf (">%s:	%i \n", dataSet[i].name.c_str(), *dataSet[i].data8);
			}
			else if (dataSet[i].type == "16")
			{
				printf (">%s:	%i \n", dataSet[i].name.c_str(), *dataSet[i].data16);
			}
			else if (dataSet[i].type == "32")
			{
				printf (">%s:	%i \n", dataSet[i].name.c_str(), *dataSet[i].data32);
			}
			else if (dataSet[i].type == "64")
			{
				printf (">%s:	%ld \n", dataSet[i].name.c_str(), *dataSet[i].data64);
			}
			else if (dataSet[i].type == "D")
			{
				printf (">%s:	%f \n", dataSet[i].name.c_str(), *dataSet[i].dataD);
			}
		}
	}
}