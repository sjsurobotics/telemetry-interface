#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include "Telemetry.h"
using namespace std;

int main()
{
	printf ("What \n");

	Telemetry *tele;
	tele = Telemetry::getInstance ();

	double test1 = 47.28;
	int8_t A = 38;
	int32_t burp = 24;

	tele -> addVar ("test1", &test1);
	tele -> addVar ("A", &A);
	tele -> addVar ("burp", &burp);

	tele -> tele("tele set A 34");
	tele -> tele("tele get A");
	tele -> tele("tele set burp 34.37");
	tele -> tele("tele set test 393.421");
	tele -> tele("tele print");

	return 0;
}	
