
const int SIZE = 10;

struct Node 
{
  String Name;
  String Type;
  int8_t *data8;
  int16_t *data16;
  int32_t *data32;
  int64_t *data64;
  double *dataD;
};

class Telemetry
{
  private:
    static Telemetry *instance;
    static int count;
    Node dataSet[SIZE];

  public:
    Telemetry ();

    void tele (String console);

    void addVar (String key, int8_t *val);
    void addVar (String key, int16_t *val);
    void addVar (String key, int32_t *val);
    void addVar (String key, int64_t *val);
    void addVar (String key, double *val);

    void getVal (String key);
    void setVal (String key, double val);

    void printAll ();
};

Telemetry* Telemetry :: instance = 0;
int Telemetry :: count = 0;

Telemetry :: Telemetry ()
{
  for (int i = 0; i < SIZE; i++)
  {
    dataSet[i].Name = "";
    dataSet[i].Type = "";
  }
}

void Telemetry :: tele (String console)
{
  char tele[100] = {' '};
  char oper[100] = {' '};
  char temp[100] = {' '};
  char numb[100] = {' '};
  double val = 0;
  String key;

  sscanf (console.c_str(), "%s %s %s %s", tele, oper, temp, numb);
  key = temp;
  val = atof(numb);

  if (!strcmp (tele, "tele"))
  {
    if (!strcmp (oper, "get")) getVal (key);
    
    else if (!strcmp (oper, "set")) setVal (key, val); 
    
    else if (!strcmp (oper, "print")) printAll ();
  }
}

void Telemetry :: addVar (String key, int8_t *val)
{
  if (count < SIZE)
  {
    dataSet[count].Name = key;
    dataSet[count].data8 = val;
    dataSet[count].Type = "8";
    Serial.print (dataSet[count].Name);
    Serial.print (" = ");
    Serial.println (*dataSet[count].data8, DEC);
    count++;
  }
}

void Telemetry :: addVar (String key, int16_t *val)
{
  if (count < SIZE)
  {
    dataSet[count].Name = key;
    dataSet[count].data16 = val;
    dataSet[count].Type = "16";
    Serial.print (dataSet[count].Name);
    Serial.print (" = ");
    Serial.println (*dataSet[count].data16, DEC);
    count++;
  }
}

void Telemetry :: addVar (String key, int32_t *val)
{
  if (count < SIZE)
  {
    dataSet[count].Name = key;
    dataSet[count].data32 = val;
    dataSet[count].Type = "32";
    Serial.print (dataSet[count].Name);
    Serial.print (" = ");
    Serial.println (*dataSet[count].data32, DEC);
    count++;
  }
}

/*
void Telemetry :: addVar (String key, int64_t *val)
{
  if (count < SIZE)
  {
    dataSet[count].Name = key;
    dataSet[count].data64 = val;
    dataSet[count].Type = "64";
    Serial.print (dataSet[count].Name);
    Serial.print (" = ");
    Serial.println (*dataSet[count].data64, DEC);
    count++;
  }
}
*/

void Telemetry :: addVar (String key, double *val) 
{
  if (count < SIZE)
  {
    dataSet[count].Name = key;
    dataSet[count].dataD = val;
    dataSet[count].Type = "D";
    Serial.print (dataSet[count].Name);
    Serial.print (" = ");
    Serial.println (*dataSet[count].dataD, 3);
    count++;
  }
}

void Telemetry :: getVal (String key)
{ 
  for (int i = 0; i < SIZE; i++)
  {
    if (dataSet[i].Name == key)
    {
      if (dataSet[i].Type == "8") Serial.println (*dataSet[i].data8, DEC);
      
      else if (dataSet[i].Type == "16") Serial.println (*dataSet[i].data16, DEC);
      
      else if (dataSet[i].Type == "32") Serial.println (*dataSet[i].data32, DEC);
      
      else if (dataSet[i].Type == "D") Serial.println (*dataSet[i].dataD, 3);
    }
  }
}

void Telemetry :: setVal (String key, double val)
{
  for (int i = 0; i < SIZE; i++)
  {
    if (dataSet[i].Name == key)
    {
      if (dataSet[i].Type == "8")
      {
        *dataSet[i].data8 = (int8_t)val;
        Serial.println (*dataSet[i].data8, DEC);
      }
      else if (dataSet[i].Type == "16")
      {
        *dataSet[i].data16 = (int16_t)val;
        Serial.println (*dataSet[i].data16, DEC);
      }
      else if (dataSet[i].Type == "32")
      {
        *dataSet[i].data32 = (int32_t)val;
        Serial.println (*dataSet[i].data32, DEC);
      }
      else if (dataSet[i].Type == "D")
      {
        *dataSet[i].dataD = val;
        Serial.println (*dataSet[i].dataD, 3);
      }
    }
  }
}

void Telemetry :: printAll ()
{
  for (int i = 0; i < SIZE; i++)
  {
    if (dataSet[i].Name != "")
    {
      if (dataSet[i].Type == "8")
      {
        Serial.print (dataSet[i].Name);
        Serial.print (" = ");
        Serial.println (*dataSet[i].data8, DEC);
      }
      else if (dataSet[i].Type == "16")
      {
        Serial.print (dataSet[i].Name);
        Serial.print (" = ");
        Serial.println (*dataSet[i].data16, DEC);        
      }
      else if (dataSet[i].Type == "32")
      {
        Serial.print (dataSet[i].Name);
        Serial.print (" = ");
        Serial.println (*dataSet[i].data32, DEC);
      }
      else if (dataSet[i].Type == "D")
      {
        Serial.print (dataSet[i].Name);
        Serial.print (" = ");
        Serial.println (*dataSet[i].dataD, 3);  
      }
    }
  }
}









Telemetry tele;
String inputString = "";
boolean stringComplete = false;

void setup ()
{
  Serial.begin(9600);
  inputString.reserve(200);
  
  int8_t A = 2;
  int16_t B = 23;
  int32_t C = 234;
  double E = 23.263;

  tele.addVar("A", &A);
  tele.addVar("B", &B);
  tele.addVar("C", &C);
  tele.addVar("E", &E);
}

void loop() 
{
  if (stringComplete)
  {
    Serial.print (inputString);
    tele.tele(inputString);
    inputString = "";
    stringComplete = false;
  }
}

void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n')
    {
      stringComplete = true;
    }
  }
}

